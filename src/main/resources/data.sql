-- Insert data into the account table
INSERT INTO account (id, account_number, account_holder_name, balance)
VALUES
    (1,'1234567890', 'John Doe', 1000.00),
    (2,'9876543210', 'Jane Smith', 2000.00),
    (3,'4567890123', 'Alice Johnson', 1500.00);

-- Insert data into the transaction table
INSERT INTO transaction (id,transaction_date, amount, account_id)
VALUES
    (1, '2023-01-15 10:30:00', 100.00, 1),
    (2, '2023-02-05 14:15:00', 50.00, 1),
    (3, '2023-02-10 09:45:00', 200.00, 2),
    (4, '2023-03-20 16:00:00', 75.00, 1),
    (5, '2023-04-03 11:30:00', 300.00, 3);

-- Insert user roles
INSERT INTO role (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO role (id, name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO users (id, username, password, account_non_expired, account_non_locked, credentials_non_expired, enabled)
VALUES (1, 'user', '$2a$12$jxtDzwBh5L606qMfiovDS.JtFnaxi.n0.SG9/XeL731SOLacRsX7O', true, true, true, true);

INSERT INTO users (id, username, password, account_non_expired, account_non_locked, credentials_non_expired, enabled)
VALUES (2, 'admin', '$2a$12$kTq1XTSG15DWOFdu5mP9FOVlu5FU1VyDcYnfWgwcP3Zpgfwh36IEG', true, true, true, true);

-- Link roles to users
INSERT INTO user_roles (user_id, role_id) VALUES (1, 1); -- user role for user
INSERT INTO user_roles (user_id, role_id) VALUES (2, 1); -- user role for admin
INSERT INTO user_roles (user_id, role_id) VALUES (2, 2); -- admin role for admin
