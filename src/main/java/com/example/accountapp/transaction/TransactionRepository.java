package com.example.accountapp.transaction;

import com.example.accountapp.account.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findByAccount(Account account);
    // You can add custom query methods if needed
}
