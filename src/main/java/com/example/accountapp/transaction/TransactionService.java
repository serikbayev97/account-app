package com.example.accountapp.transaction;

import com.example.accountapp.account.Account;
import com.example.accountapp.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountService accountService;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository, AccountService accountService) {
        this.transactionRepository = transactionRepository;
        this.accountService = accountService;
    }

    @Transactional
    public Transaction createTransaction(Account account, Transaction transaction) {
        BigDecimal amount = account.getBalance().subtract(transaction.getAmount());
        if (amount.compareTo(BigDecimal.ZERO) >= 0) {
            account.setBalance(amount);
        } else {
            throw new RuntimeException("Insufficient funds in the account.");
        }

        transactionRepository.save(transaction);
        accountService.updateAccount(account.getId(), account);
        return transaction;
    }

    public List<Transaction> getAllTransactionsByAccount(Account account) {
        return transactionRepository.findByAccount(account);
    }

    public Optional<Transaction> getTransactionById(Long id) {
        return transactionRepository.findById(id);
    }


    // TODO check account for balance availability
    @Transactional
    public Transaction updateTransaction(Long id, Transaction updatedTransaction) {
        // Perform any business logic validation if needed
        Optional<Transaction> existingTransaction = transactionRepository.findById(id);
        if (existingTransaction.isPresent()) {
            // Update transaction fields
            Transaction transactionToUpdate = existingTransaction.get();
            // Assuming you want to update specific fields, e.g., amount and description
            transactionToUpdate.setAmount(updatedTransaction.getAmount());
            transactionToUpdate.setTransactionDate(updatedTransaction.getTransactionDate());
            return transactionRepository.save(transactionToUpdate);
        } else {
            return null; // Transaction not found
        }
    }

    public boolean deleteTransaction(Long id) {
        // Perform any business logic validation if needed
        Optional<Transaction> existingTransaction = transactionRepository.findById(id);
        if (existingTransaction.isPresent()) {
            transactionRepository.delete(existingTransaction.get());
            return true;
        } else {
            return false; // Transaction not found
        }
    }
}
