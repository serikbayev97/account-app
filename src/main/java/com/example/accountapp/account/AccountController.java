package com.example.accountapp.account;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/accounts")
@Api(tags = "Account Management") // Added Swagger annotation
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    @Autowired
    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @ApiOperation(value = "Get all accounts")
    @GetMapping
    public List<Account> getAllAccounts() {
        logger.info("Getting all accounts");
        List<Account> accounts = accountService.getAllAccounts();
        logger.debug("Found {} accounts", accounts.size());
        return accounts;
    }

    @ApiOperation(value = "Get an account by ID")
    @GetMapping("/{id}")
    public ResponseEntity<Account> getAccountById(@PathVariable Long id) {
        logger.info("Getting account by ID: {}", id);
        Optional<Account> account = accountService.getAccountById(id);
        if (account.isPresent()) {
            Account foundAccount = account.get();
            return new ResponseEntity<>(foundAccount, HttpStatus.OK);
        } else {
            logger.warn("Account not found for ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Create an account")
    @PostMapping
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        logger.info("Creating account: {}", account);
        Account savedAccount = accountService.createAccount(account);
        logger.info("Account created with ID: {}", savedAccount.getId());
        return new ResponseEntity<>(savedAccount, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update an account by ID")
    @PutMapping("/{id}")
    public ResponseEntity<Account> updateAccount(@PathVariable Long id, @RequestBody Account updatedAccount) {
        logger.info("Updating account with ID: {}", id);
        Optional<Account> updated = accountService.updateAccount(id, updatedAccount);
        if (updated.isPresent()) {
            logger.info("Account updated: {}", updated);
            return new ResponseEntity<>(updated.get(), HttpStatus.OK);
        } else {
            logger.warn("Account not found for update with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Delete an account by ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAccount(@PathVariable Long id) {
        logger.info("Deleting account with ID: {}", id);
        boolean deleted = accountService.deleteAccount(id);
        if (deleted) {
            logger.info("Account deleted with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            logger.warn("Account not found for delete with ID: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
