package com.example.accountapp.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Cacheable("accounts")
    public Optional<Account> getAccountById(Long id) {
        return accountRepository.findById(id);
    }

    public Account createAccount(Account account) {
        // You can add validation or business logic here before saving
        return accountRepository.save(account);
    }

    public Optional<Account> updateAccount(Long id, Account updatedAccount) {
        Optional<Account> existingAccount = accountRepository.findById(id);
        if (existingAccount.isPresent()) {
            // You can update fields of the existing account here
            Account accountToUpdate = existingAccount.get();
            accountToUpdate.setAccountNumber(updatedAccount.getAccountNumber());
            accountToUpdate.setAccountHolderName(updatedAccount.getAccountHolderName());
            accountToUpdate.setBalance(updatedAccount.getBalance());
            // Update other fields as needed
            return Optional.of(accountRepository.save(accountToUpdate));
        }
        return Optional.empty();
    }

    public boolean deleteAccount(Long id) {
        if (accountRepository.existsById(id)) {
            accountRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
