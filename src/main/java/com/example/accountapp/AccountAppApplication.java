package com.example.accountapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class AccountAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountAppApplication.class, args);
    }

}
